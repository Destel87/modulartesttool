#include "password.h"
#include "ui_password.h"
#include "version.h"
#define PWD "1234"

Password::Password(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Password)
{
    ui->setupUi(this);
    _bPwd = false;

    QString strTitle;
    strTitle.append(TITLE);
    strTitle.append(VERSION);
    this->setWindowTitle(strTitle);
}

Password::~Password()
{
    delete ui;
}

void Password::on_pushButton_OK_clicked()
{
    QString sPwd;
    sPwd = ui->lineEditPwd->text();

    _bPwd = false;
    if ( sPwd == PWD)
    {
        _bPwd = true;
    }

    ui->lineEditPwd->clear();
    this->close();
}

bool Password::getPwd()
{
    return _bPwd;
}

void Password::on_pushButton_Cancel_clicked()
{
    _bPwd = false;
    ui->lineEditPwd->clear();
    this->close();
}
