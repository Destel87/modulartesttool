#ifndef VOLTAGETHREAD_H
#define VOLTAGETHREAD_H

#include <QObject>
#include <QWidget>
#include <QThread>

#define UPDATE_V_MS 200

class VoltageThread : public QThread
{
    Q_OBJECT
public:
    VoltageThread();
    void run();
    void stopThread(void);


signals:
    void requestVoltage(int);

public slots:

public:
    bool _StopThread;
    int _voltage;

};

#endif // VOLTAGETHREAD_H
