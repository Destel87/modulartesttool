/*! ****************************************************************************
 @copyright
 bioMerieux Italy - Confidential & proprietary intellectual property
 Copyright (C) 2020 bioMerieux, Inc. This program is the property of
 bioMerieux, Inc, its contents are proprietary information and no part of it
 is to be disclosed to anyone except employees of bioMerieux, Inc or as
 explicitly agreed in writing with a Statement of Non-Disclosure.

 @file    swap.h
 @author  BmxIta FW dept
 @brief   Contains the functions for the swap functions.
 @details

 ****************************************************************************
*/

#ifndef _SWAP_H_
#define _SWAP_H_

#include <unistd.h>
#include <stdint.h>
#include <string.h>

#ifdef __cplusplus  /* extern "C" */
extern "C" {
#endif

void Swap32(unsigned char *ptuLong);
void Swap16(unsigned char *ptuShort);
uint16_t swap16Linux(uint16_t pusData);

#ifdef __cplusplus  /* extern "C" */
}
#endif
#endif
