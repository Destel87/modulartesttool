#include "consolewindow.h"
#include "ui_consolewindow.h"

#include "console.h"
#include "settingsdialog.h"

#include <QMessageBox>
#include <QLabel>
#include <QtSerialPort/QSerialPort>

ConsoleWindow::ConsoleWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ConsoleWindow)
{    
    ui->setupUi(this);
    _active = false;
 //Create a console widget
    console = new Console;
    console->setEnabled(false);
    setCentralWidget(console);

 //Create serial and settings widgets
    serial = new QSerialPort(this);
    settings = new SettingsDialog;

    ui->actionConnect->setEnabled(true);
    ui->actionDisconnect->setEnabled(false);
    ui->actionQuit->setEnabled(true);
    ui->actionConfigure->setEnabled(true);

    status = new QLabel;
    ui->statusBar->addWidget(status);

    initActionsConnections();

    connect(serial, static_cast<void (QSerialPort::*)(QSerialPort::SerialPortError)>(&QSerialPort::error),
            this, &ConsoleWindow::handleError);

    connect(serial, &QSerialPort::readyRead, this, &ConsoleWindow::readData);

    connect(console, &Console::getData, this, &ConsoleWindow::writeData);
}

ConsoleWindow::~ConsoleWindow()
{
    delete settings;
    delete ui;
}

void ConsoleWindow::writeData(const QByteArray &data)
{
    serial->write(data);
}

void ConsoleWindow::readData()
{
    QByteArray data = serial->readAll();
    console->putData(data);
}

void ConsoleWindow::initActionsConnections()
{
    connect(ui->actionConnect, &QAction::triggered, this, &ConsoleWindow::openSerialPort);
    connect(ui->actionDisconnect, &QAction::triggered, this, &ConsoleWindow::closeSerialPort);
    connect(ui->actionQuit, &QAction::triggered, this, &ConsoleWindow::close);
    connect(ui->actionConfigure, &QAction::triggered, settings, &SettingsDialog::show);
    connect(ui->actionClear, &QAction::triggered, console, &Console::clear);
    connect(ui->actionAbout, &QAction::triggered, this, &ConsoleWindow::about);
    connect(ui->actionAboutQt, &QAction::triggered, qApp, &QApplication::aboutQt);
}

void ConsoleWindow::showStatusMessage(const QString &message)
{
    status->setText(message);
}

void ConsoleWindow::openSerialPort()
{
    SettingsDialog::Settings p = settings->settings();
    serial->setPortName(p.name);
    serial->setBaudRate(p.baudRate);
    serial->setDataBits(p.dataBits);
    serial->setParity(p.parity);
    serial->setStopBits(p.stopBits);
    serial->setFlowControl(p.flowControl);
    if (serial->open(QIODevice::ReadWrite)) {
        console->setEnabled(true);
        console->setLocalEchoEnabled(p.localEchoEnabled);
        ui->actionConnect->setEnabled(false);
        ui->actionDisconnect->setEnabled(true);
        ui->actionConfigure->setEnabled(false);
        showStatusMessage(tr("Connected to %1 : %2, %3, %4, %5, %6")
                          .arg(p.name).arg(p.stringBaudRate).arg(p.stringDataBits)
                          .arg(p.stringParity).arg(p.stringStopBits).arg(p.stringFlowControl));
    }
    else
    {
        QMessageBox::critical(this, tr("Error"), serial->errorString());

        showStatusMessage(tr("Open error"));
    }
}

void ConsoleWindow::closeSerialPort()
{
    if (serial->isOpen())
        serial->close();
    console->setEnabled(false);
    ui->actionConnect->setEnabled(true);
    ui->actionDisconnect->setEnabled(false);
    ui->actionConfigure->setEnabled(true);
    showStatusMessage(tr("Disconnected"));
}

void ConsoleWindow::about()
{
    QMessageBox::about(this, tr("About Console"),
                       tr("The <b>Simple Terminal</b> example demonstrates how to "
                          "use the Qt Serial Port module in modern GUI applications "
                          "using Qt, with a menu bar, toolbars, and a status bar."));
}

void ConsoleWindow::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        QMessageBox::critical(this, tr("Critical Error"), serial->errorString());
        closeSerialPort();
    }
}

void ConsoleWindow::setActive(bool bVal)
{
    _active = bVal;
}

bool ConsoleWindow::getActive()
{
    return _active;
}
